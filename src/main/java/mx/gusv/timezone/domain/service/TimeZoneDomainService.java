package mx.gusv.timezone.domain.service;

import mx.gusv.timezone.domain.model.TimeZoneAggregate;
import mx.gusv.timezone.domain.model.TimeZoneId;
import mx.gusv.timezone.repository.TimeZoneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class TimeZoneDomainService {

    /***/
    private final TimeZoneRepository repository;

    @Autowired
    public TimeZoneDomainService(final TimeZoneRepository repository) {
        this.repository = repository;
    }

    @Transactional
    void addTimeZone(final String timeZoneId) {
        TimeZoneId id = new TimeZoneId(timeZoneId);
        Optional<TimeZoneAggregate> actual = repository.findById(id);
        if (actual.isPresent()) {
            throw new IllegalArgumentException("The timezone with id " + timeZoneId + " already exists");
        }

        TimeZoneAggregate entity = new TimeZoneAggregate(id);
        repository.save(entity);
    }

    @Transactional
    void cancelDst(final String timeZoneId) {
        TimeZoneId id = new TimeZoneId(timeZoneId);
        TimeZoneAggregate timeZone = repository.findById(id).orElseThrow(
                () -> new IllegalArgumentException("Time timezone with id " + timeZoneId + " does not exist"));

        timeZone.disableDst();
    }
}
