package mx.gusv.timezone.domain.model;

public enum OccurrenceInMonth {

    /***/
    FIRST(0),
    /***/
    SECOND(1),
    /***/
    THIRD(2),
    /***/
    FOURTH(3),
    /***/
    LAST(0);

    /***/
    private int weeksToAdd;

    OccurrenceInMonth(final int weeksToAdd) {
        this.weeksToAdd = weeksToAdd;
    }

    public int getWeeksToAdd() {
        return weeksToAdd;
    }
}
