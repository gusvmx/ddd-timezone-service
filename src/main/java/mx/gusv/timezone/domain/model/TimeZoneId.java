package mx.gusv.timezone.domain.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class TimeZoneId implements Serializable {

    /***/
    private static final long serialVersionUID = 2693591755061828460L;
    /***/
    @Column(name = "timezone_id")
    @NotNull
    private String id;

    /** Required by JPA. */
    protected TimeZoneId() { }
    public TimeZoneId(final String id) {
        // Se garantiza que un id se genere correctamente en el constructor.
        if (id.split("/").length != 2) {
            throw new IllegalArgumentException(
                    "El id de la zona debe ser en el formato Continene/ciudad. Recibimos el valor de " + id);
        }
        this.id = id;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TimeZoneId that = (TimeZoneId) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
