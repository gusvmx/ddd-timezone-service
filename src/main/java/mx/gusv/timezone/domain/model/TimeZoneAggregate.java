package mx.gusv.timezone.domain.model;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.time.LocalDateTime;
import java.time.Year;

@Entity(name = "timezone")
public class TimeZoneAggregate {

    /***/
    @EmbeddedId
    private TimeZoneId id;
    /***/
    @Column(name = "dst_enabled")
    private boolean dstEnabled;
    /***/
    @Embedded
    @AttributeOverride(name = "dayOfWeek", column = @Column(name = "dst_start_day_of_week"))
    @AttributeOverride(name = "month", column = @Column(name = "dst_start_month"))
    @AttributeOverride(name = "occurrenceInMonth", column = @Column(name = "dst_start_occurrence"))
    @AttributeOverride(name = "localTime", column = @Column(name = "dst_start_local_time"))
    private SwitchDateTime dstStartDatetime;
    /***/
    @Embedded
    @AttributeOverride(name = "dayOfWeek", column = @Column(name = "dst_end_day_of_week"))
    @AttributeOverride(name = "month", column = @Column(name = "dst_end_month"))
    @AttributeOverride(name = "occurrenceInMonth", column = @Column(name = "dst_end_occurrence"))
    @AttributeOverride(name = "localTime", column = @Column(name = "dst_end_local_time"))
    private SwitchDateTime dstEndDatetime;


    /**
     * Constructor que aseura se generen instancias con lo minimo necesario. Garantizando que se generen correctamente
     * @param id
     */
    public TimeZoneAggregate(final TimeZoneId id) {
        this.id = id;
        this.dstEnabled = false;
    }

    public TimeZoneAggregate(final TimeZoneId id, final SwitchDateTime startDatetime,
                             final SwitchDateTime endDatetime) {
        this(id);
        this.dstEnabled = true;
        this.dstStartDatetime = startDatetime;
        this.dstEndDatetime = endDatetime;
    }

    /***/
    protected TimeZoneAggregate() {

    }

    /**
     * Incorpora lenguaje ubicuo, que representa una accion de negocio.
     */
    public void enableDst() {
        if (dstStartDatetime == null || dstEndDatetime == null) {
            throw new IllegalStateException(
                    "Either start date time or end date time is null, or both. "
                            + "To enable DST, both start and end date times are required");
        }
        this.dstEnabled = true;
    }

    public void disableDst() {
        this.dstEnabled = false;
    }

    /**
     * Alcance default para evitar que se accedan a los valores de las propiedades mediante getters.
     * Prefiriendo tell don't ask
     * @return Indica si la zona horaria cuenta con DST o no.
     */
    public boolean isDstEnabled() {
        return dstEnabled;
    }

    public void changeStartDatetime(final SwitchDateTime switchDateTime) {
        this.dstStartDatetime = switchDateTime;
    }

    public boolean isDstActive(final LocalDateTime dateTime) {
        if (!isDstEnabled()) {
            return false;
        }
        final Year year = Year.of(dateTime.getYear());
        LocalDateTime start = dstStartDatetime.getDatetime(year);
        LocalDateTime end = dstEndDatetime.getDatetime(year);
        return dateTime.isAfter(start) && dateTime.isBefore(end);
    }
}
