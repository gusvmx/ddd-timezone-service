package mx.gusv.timezone.domain.model;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.Year;
import java.time.YearMonth;

@Embeddable
public class SwitchDateTime {

    /***/
    @Enumerated(EnumType.STRING)
    private DayOfWeek dayOfWeek;
    /***/
    @Enumerated(EnumType.STRING)
    private Month month;
    /***/
    @Enumerated(EnumType.STRING)
    private OccurrenceInMonth occurrenceInMonth;
    /***/
    private LocalTime localTime;

    public SwitchDateTime(final DayOfWeek dayOfWeek, final Month month, final OccurrenceInMonth occurrenceInMonth,
                          final LocalTime localTime) {
        this.dayOfWeek = dayOfWeek;
        this.month = month;
        this.occurrenceInMonth = occurrenceInMonth;
        this.localTime = localTime;
    }

    /** Required by JPA. */
    protected SwitchDateTime() { }

    public LocalDateTime getDatetime(final Year of) {
        YearMonth ym = YearMonth.of(of.getValue(), month.getValue());
        LocalDate date = null;
        if (occurrenceInMonth == OccurrenceInMonth.LAST) {
            date = findDayOfWeekDate(ym.atEndOfMonth(), dayOfWeek, -1, occurrenceInMonth.getWeeksToAdd());
        } else {
            date = findDayOfWeekDate(ym.atDay(1), dayOfWeek, 1, occurrenceInMonth.getWeeksToAdd());
        }
        return date.atTime(localTime);
    }

    private LocalDate findDayOfWeekDate(final LocalDate localDate, final DayOfWeek dayOfWeek, final int daysToAdd,
                                        final int weeksToAdd) {
        if (localDate.getDayOfWeek() == dayOfWeek) {
            if (weeksToAdd > 0) {
                return findDayOfWeekDate(localDate.plusDays(daysToAdd), dayOfWeek, daysToAdd, weeksToAdd - 1);
            }
            return localDate;
        }
        return findDayOfWeekDate(localDate.plusDays(daysToAdd), dayOfWeek, daysToAdd, weeksToAdd);
    }
}
