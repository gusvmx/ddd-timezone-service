/**
 * 
 */
package mx.gusv.timezone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author gusvmx
 *
 */
@SpringBootApplication
public class TimeZoneServiceApplication {

    /***/
    private static final Logger LOGGER = LoggerFactory.getLogger(TimeZoneServiceApplication.class);
    
    /**
     * @param args
     */
    public static void main(final String[] args) {
        SpringApplication.run(TimeZoneServiceApplication.class);
        LOGGER.info("Aplicacion inicializada");
    }

}
