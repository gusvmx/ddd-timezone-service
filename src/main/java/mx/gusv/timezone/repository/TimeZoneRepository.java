package mx.gusv.timezone.repository;

import mx.gusv.timezone.domain.model.TimeZoneAggregate;
import mx.gusv.timezone.domain.model.TimeZoneId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TimeZoneRepository extends JpaRepository<TimeZoneAggregate, TimeZoneId> {

}
