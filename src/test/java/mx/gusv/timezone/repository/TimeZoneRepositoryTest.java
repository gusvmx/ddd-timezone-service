package mx.gusv.timezone.repository;

import mx.gusv.timezone.domain.model.OccurrenceInMonth;
import mx.gusv.timezone.domain.model.SwitchDateTime;
import mx.gusv.timezone.domain.model.TimeZoneAggregate;
import mx.gusv.timezone.domain.model.TimeZoneId;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.util.Optional;

@SpringBootTest
@Transactional
class TimeZoneRepositoryTest {

    @Autowired
    private TimeZoneRepository repository;

    @Test
    void saveTimeZone() {
        SwitchDateTime startDateTime = new SwitchDateTime(DayOfWeek.SUNDAY, Month.APRIL, OccurrenceInMonth.FIRST, LocalTime.of(2, 0));
        SwitchDateTime endDateTime = new SwitchDateTime(DayOfWeek.SUNDAY, Month.OCTOBER, OccurrenceInMonth.LAST, LocalTime.of(2, 0));
        TimeZoneAggregate aggregate = new TimeZoneAggregate(new TimeZoneId("America/Mexico_city"), startDateTime, endDateTime);

        repository.save(aggregate);

        Optional<TimeZoneAggregate> actual = repository.findById(new TimeZoneId("America/Mexico_city"));
        Assertions.assertTrue(actual.isPresent());
        boolean isActive = actual.get().isDstActive(LocalDateTime.of(2022, 10, 23, 0, 0));
        Assertions.assertTrue(isActive);
    }
}