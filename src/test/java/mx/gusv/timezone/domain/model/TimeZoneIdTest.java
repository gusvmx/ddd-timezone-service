package mx.gusv.timezone.domain.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

class TimeZoneIdTest {

    @Test
    void invalidId() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> new TimeZoneId("Mexico_city"));
    }
    @Test
    void equalsSameInstance() {
        TimeZoneId id = new TimeZoneId("America/Mexico_city");

        Assertions.assertEquals(id, id);
    }

    @Test
    void equalsSameId() {
        TimeZoneId id = new TimeZoneId("America/Mexico_city");
        TimeZoneId secondId = new TimeZoneId("America/Mexico_city");

        Assertions.assertEquals(id, secondId);
    }

    @Test
    void notEqualsDiffClass() {
        TimeZoneId id = new TimeZoneId("America/Mexico_city");
        String string = "America/Mexico_city";

        Assertions.assertNotEquals(id, string);
    }

    @Test
    void equalsHashCode() {
        TimeZoneId id = new TimeZoneId("America/Mexico_city");
        Set<TimeZoneId> ids = new HashSet<>();
        ids.add(id);

        Assertions.assertTrue(ids.contains(id));
    }

}