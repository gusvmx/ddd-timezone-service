package mx.gusv.timezone.domain.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.time.*;

import static org.junit.jupiter.api.Assertions.*;

class SwitchDateTimeTest {

    @Test
    void getSwitchDatetimeForLastSundayOfMonth() {
        SwitchDateTime switchDateTime = new SwitchDateTime(DayOfWeek.SUNDAY, Month.OCTOBER, OccurrenceInMonth.LAST, LocalTime.of(2, 0));

        LocalDateTime localDateTime = switchDateTime.getDatetime(Year.of(2022));

        Assertions.assertEquals(LocalDateTime.of(2022, 10, 30, 2, 0), localDateTime);
    }

    @Test
    void getSwitchDatetimeForFirstSundayOfMonth() {
        SwitchDateTime switchDateTime = new SwitchDateTime(DayOfWeek.SUNDAY, Month.APRIL, OccurrenceInMonth.FIRST, LocalTime.of(2, 0));

        LocalDateTime localDateTime = switchDateTime.getDatetime(Year.of(2022));

        Assertions.assertEquals(LocalDateTime.of(2022, 4, 3, 2, 0), localDateTime);
    }

    @ParameterizedTest
    @CsvSource({
            "MONDAY, FIRST, 4",
            "MONDAY, SECOND, 11",
            "MONDAY, THIRD, 18",
            "MONDAY, FOURTH, 25"
    })
    void getSwitchDateForNMondayOfMonth(final DayOfWeek dayOfWeek, final OccurrenceInMonth occurrenceInMonth, final int expectedDayOfMonth) {
        SwitchDateTime switchDateTime = new SwitchDateTime(dayOfWeek, Month.APRIL, occurrenceInMonth, LocalTime.of(2,0));

        LocalDateTime localDateTime = switchDateTime.getDatetime(Year.of(2022));

        Assertions.assertEquals(LocalDateTime.of(2022, Month.APRIL.getValue(), expectedDayOfMonth, 2, 0), localDateTime);
    }
}