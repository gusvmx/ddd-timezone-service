package mx.gusv.timezone.domain.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;

class TimeZoneAggregateTest {

    @Test
    void enableDst() {
        SwitchDateTime startDateTime = new SwitchDateTime(DayOfWeek.SUNDAY, Month.APRIL, OccurrenceInMonth.FIRST, LocalTime.of(2, 0));
        SwitchDateTime endDateTime = new SwitchDateTime(DayOfWeek.SUNDAY, Month.OCTOBER, OccurrenceInMonth.LAST, LocalTime.of(2, 0));
        TimeZoneAggregate aggregate = new TimeZoneAggregate(new TimeZoneId("America/Mexico_city"), startDateTime, endDateTime);

        aggregate.enableDst();

        Assertions.assertTrue(aggregate.isDstEnabled());
    }

    @Test
    void enableDstWithoutStartDatetimeThrowsAnException() {
        TimeZoneAggregate aggregate = new TimeZoneAggregate(new TimeZoneId("America/Mexico_city"));

        Assertions.assertThrows(IllegalStateException.class, () -> aggregate.enableDst());
    }

    @Test
    void enableDstWithoutEndDatetimeThrowsAnException() {
        TimeZoneAggregate aggregate = new TimeZoneAggregate(new TimeZoneId("America/Mexico_city"));
        SwitchDateTime switchDateTime = new SwitchDateTime(DayOfWeek.SUNDAY, Month.APRIL, OccurrenceInMonth.FIRST, LocalTime.of(2, 0));
        aggregate.changeStartDatetime(switchDateTime);

        Assertions.assertThrows(IllegalStateException.class, () -> aggregate.enableDst());
    }

    @Test
    void disabledDst() {
        TimeZoneAggregate aggregate = new TimeZoneAggregate(new TimeZoneId("America/Mexico_city"));

        aggregate.disableDst();

        Assertions.assertFalse(aggregate.isDstEnabled());
    }

    @Test
    void isNotActiveWhenDstIsDisabled() {
        TimeZoneAggregate aggregate = new TimeZoneAggregate(new TimeZoneId("America/Mexico_city"));

        boolean isActive = aggregate.isDstActive(LocalDateTime.now());

        Assertions.assertFalse(isActive);
    }

    @Test
    void isActive() {
        SwitchDateTime startDateTime = new SwitchDateTime(DayOfWeek.SUNDAY, Month.APRIL, OccurrenceInMonth.FIRST, LocalTime.of(2, 0));
        SwitchDateTime endDateTime = new SwitchDateTime(DayOfWeek.SUNDAY, Month.OCTOBER, OccurrenceInMonth.LAST, LocalTime.of(2, 0));
        TimeZoneAggregate aggregate = new TimeZoneAggregate(new TimeZoneId("America/Mexico_city"), startDateTime, endDateTime);

        boolean isActive = aggregate.isDstActive(LocalDateTime.of(2022, 9, 16, 0, 0));

        Assertions.assertTrue(isActive);
    }

    @Test
    void isNotActive() {
        SwitchDateTime startDateTime = new SwitchDateTime(DayOfWeek.SUNDAY, Month.APRIL, OccurrenceInMonth.FIRST, LocalTime.of(2, 0));
        SwitchDateTime endDateTime = new SwitchDateTime(DayOfWeek.SUNDAY, Month.OCTOBER, OccurrenceInMonth.LAST, LocalTime.of(2, 0));
        TimeZoneAggregate aggregate = new TimeZoneAggregate(new TimeZoneId("America/Mexico_city"), startDateTime, endDateTime);

        boolean isActive = aggregate.isDstActive(LocalDateTime.of(2022, 11, 16, 0, 0));

        Assertions.assertFalse(isActive);
    }
}