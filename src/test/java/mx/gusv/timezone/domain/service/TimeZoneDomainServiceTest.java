package mx.gusv.timezone.domain.service;

import mx.gusv.timezone.domain.model.TimeZoneAggregate;
import mx.gusv.timezone.domain.model.TimeZoneId;
import mx.gusv.timezone.repository.TimeZoneRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;
import java.util.Optional;

@SpringBootTest
@Transactional
class TimeZoneDomainServiceTest {

    public static final String AMERICA_MEXICO_CITY = "America/Mexico_city";
    @Autowired
    private TimeZoneDomainService domainService;
    @Autowired
    private TimeZoneRepository repository;

    @Test
    void addNewTimeZone() {
        domainService.addTimeZone(AMERICA_MEXICO_CITY);

        Optional<TimeZoneAggregate> actual = repository.findById(new TimeZoneId(AMERICA_MEXICO_CITY));
        Assertions.assertTrue(actual.isPresent());
    }

    @Test
    void addDuplicateTImeZone() {
        //Given
        domainService.addTimeZone(AMERICA_MEXICO_CITY);
        //When
        Assertions.assertThrows(IllegalArgumentException.class, () -> domainService.addTimeZone(AMERICA_MEXICO_CITY));
    }

    @Test
    void cancelDst() {
        domainService.addTimeZone(AMERICA_MEXICO_CITY);

        domainService.cancelDst(AMERICA_MEXICO_CITY);

        TimeZoneAggregate actual = repository.findById(new TimeZoneId(AMERICA_MEXICO_CITY)).orElseThrow();
        Assertions.assertFalse(actual.isDstEnabled());
    }

    @Test
    void cancelDstForInvalidTimeZone() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> domainService.cancelDst(AMERICA_MEXICO_CITY));
    }
}